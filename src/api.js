import { findIndex } from "lodash"
export function getCriminals() {
  return delay().then(() => {
    return database
  })
}

export function addCriminal(criminalObj) {
  const { id, name, crime } = criminalObj
  if (id && name && crime) {
    return delay().then(() => {
      const existing = findIndex(database, item => item.id)
      if (existing !== -1) {
        database.push(criminalObj)
        return delay().then(() => criminalObj)
      } else {
        return delay().then(() => Promise.reject(`existing entry of the same id`))
      }
    })
  } else {
    return delay().then(() => Promise.reject(`missing required property id:${id}, name:${name}, crime:${crime}`))
  }
}

export function patchCriminal(id, partial) {
  if (id) {
    const existing = findIndex(database, item => item.id)
    if (existing !== -1) {
      database[existing] = { ...database[existing], ...partial }
      return delay().then(() => database[existing])
    } else {
      return delay().then(() => Promise.reject(`cannot patch id: ${id}`))
    }
  }
}

const database = [
  { id: "1", name: "Han Solo", crime: "smuggler" },
  { id: "2", name: "Luke Skywalker", crime: "rebel" },
]

function delay(ms = 1000) {
  return new Promise(r => {
    setTimeout(r, ms)
  })
}

// function randomDelay(min = 100, max= 500) {
//   return Math.floor(Math.random() * (max - min + 1) + min)
// }
