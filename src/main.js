import { imperialBackground } from "imperial-style"
import "imperial-style/imperial.css"
import Vue from "vue"
import App from "./App.vue"
import VueRouter from "vue-router"
const ExistingCriminals = () => import("./routes/existing-criminals/existing-criminals.vue")
const CreateCriminal = () => import("./routes/create-criminal/create-criminal.vue")

imperialBackground({ animate: false })

Vue.config.productionTip = false
Vue.use(VueRouter)

const routes = [
  {
    path: "/existing",
    component: ExistingCriminals,
    name: "existing",
  },
  {
    path: "/create-new",
    name: "create-new",
    component: CreateCriminal,
  },
]

const router = new VueRouter({
  mode: "history",
  routes,
})

new Vue({
  render: h => h(App),
  router,
}).$mount("#app")
